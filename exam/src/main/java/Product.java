import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Product extends JFrame implements ActionListener {
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton button1;

    public Product() {
        setLayout(new FlowLayout());
        setSize(600, 600);

        textField1 = new JTextField(8);
        add(textField1);

        textField2 = new JTextField(8);
        add(textField2);

        textField3 = new JTextField(8);
        textField3.setEditable(false);
        add(textField3);

        button1 = new JButton("Click!");
        add(button1);
        button1.addActionListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int a = Integer.parseInt(textField1.getText());
        int b = Integer.parseInt(textField2.getText());
        int c = a * b;
        textField3.setText(Integer.toString(c));
    }

    public static void main(String[] args) {
        Product product = new Product();
        product.setVisible(true);
    }
}
/* Diagram
Class B{
private String param;
public E e;
public void function(Z z);}
Class A extends B{}
Class C{
public B b;
C(){
B b=new B();
}
Class D{
private B b;
public void f();
}
Class E{}
Class Z{
public void g();}
*/
